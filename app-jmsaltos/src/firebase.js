
import firebase from 'firebase/app'
import 'firebase/firestore'

var firebaseConfig = {
  apiKey: "AIzaSyBgmyHZqrB2wlux8AkVTpMRC_ktUyaN1JU",
    authDomain: "app-jmsaltos.firebaseapp.com",
    databaseURL: "https://app-jmsaltos.firebaseio.com",
    projectId: "app-jmsaltos",
    storageBucket: "app-jmsaltos.appspot.com",
    messagingSenderId: "132240361007",
    appId: "1:132240361007:web:ca29eb35b3a5ce6195e8da",
    measurementId: "G-1XB8PEL6K5"
  };
  // Initialize Firebase
  const fb = firebase.initializeApp(firebaseConfig);
  export const db = fb.firestore();