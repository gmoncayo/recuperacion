import React, {useState, useEffect} from 'react';
import { db } from '../firebase';

const ReservasForm = (props)=> {

    const initialStateValues={
        placaReserva: '',
        NumeroReserva: '',
        email:'',
        celular:'',
        fechaPrestamo:'',
        fechaEntrega: '',
        valor: ''
    };
    const [values, setValues] = useState(initialStateValues);

    var numIncorr = 0;
    var msgErrorComp = 0;
    var msgDescuento = 0;
    const handleInputChange = e => {
        const {id, value}  =e.target;
        setValues({...values, [id]: value});     
    };
    var isDisp = '';
    const listPlacas = [];
    const handleSubmit = e =>{
        e.preventDefault();
        // calculoDias();
        //validarNUmeroTelefono();
        EscogAuto();
        let isVer = false;
        let reservaRef = db.collection('Reservas');
        const docs = [];
        const lstPlcsRsrv = [];
        var tam = 0;
        values.NumeroReserva = numReserva(100,1000);
        reservaRef.onSnapshot((querySnapshot) => {            
            querySnapshot.forEach((doc) => {
                docs.push(doc.data().NumeroReserva);
                lstPlcsRsrv.push(doc.data().placaReserva);
                console.log(doc.data().NumeroReserva);
                tam = tam+1;              
        });
            let inpNumRes = values.NumeroReserva;
            let i = 0;
            
                for(i = 0; i < tam; i++){
                    let act = docs[i];
                if(inpNumRes == act){                    
                    inpNumRes = numReserva(100,1000);
                    return;            
                 }if(inpNumRes != act && values.placaReserva != lstPlcsRsrv[i]){
                //}if(inpNumRes != act){
                    if(validarNUmeroTelefono()){
                    isVer = true;
                    numIncorr = 1;
                    console.log("la lista es DE PLACAS A COMPARAR SON: " + [lstPlcsRsrv]);
                    }else{
                        numIncorr = 0;
                    }
                }
                if(values.placaReserva == lstPlcsRsrv[i]){
                    isVer = false;
                    
                    return;
                }
            }
            
            if(msgErrorComp == 1){
            alert('NO EXISTE VEHÍCULO CON ESA PLACA');
            msgErrorComp = 0;
            return;
             }

            if(numIncorr == 0){
                alert('NÚMERO DE TELÉFONO INCORRECTO');
                return;
            }            

            if(msgDescuento == 1){
                alert('OBTIENE EL 10% DE DESCUENTO');
                msgDescuento = 0;
            }      

            if(tam == 0){
                isVer = true;
            }
            console.log('el bool es: '+isVer);  
            console.log('isDisp es: ' + isDisp);
            
               if((isVer||tam==0))
                {
                    if(isDisp==true){
                        values.NumeroReserva = inpNumRes;
                        props.addOrEdit(values);
                        setValues({...initialStateValues});
                        isVer = false;                    
                        i = tam+1;                    
                        return;
                    }
                }
                 if(!isVer){
                    alert('YA EXISTE UNA RESERVA');
                    return;
                }   
            });
            
    }

    const getReservaById = async (id) =>{
        const doc = await db.collection('Reservas').doc(id).get();
        setValues({...doc.data()})
    }

    useEffect(()=>{
        if(props.currentId === ''){
            setValues({...initialStateValues});
        }else{
            getReservaById(props.currentId);
        }
    },[props.currentId])

    //----------------------------------------------

    function validarNUmeroTelefono(){
        if(values.celular.length <= 10 && values.celular.length != 0){
            if(values.celular.charAt(0) == 0 && values.celular.charAt(1) == 9){
                return true;
            }else{
                return '';
            }            
        }else{
            return '';
        }        
    }

    function numReserva(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
      }
    
    function EscogAuto(){
        let vehiculoRef = db.collection('Vehiculos');
        // const listPlacas = [];
        //const listDisp = [];
        const listId = [];
        const listCosto = [];
        var tamList = 0; 
        
        vehiculoRef.onSnapshot((querySnapshot) => {            
            querySnapshot.forEach((doc) => {
                listPlacas.push(doc.data().placa);
                // listDisp.push(doc.data().disponible);
                listId.push(doc.id);
                listCosto.push(doc.data().costo);
                tamList = tamList+1;

        });
        let placaIngresada = values.placaReserva;
        //console.log('placa ingresada: ' + placaIngresada);
        //console.log('las placas son: ' + [listPlacas]);
        //console.log('las disponibles son: ' + [listDisp]);        
        
        let i = 0;
        for(i = 0; i<tamList; i++){
            //console.log('la placa a select es: ' + listPlacas[i]);
            //console.log('la placas son: ' + [listPlacas]);
            var aComparar = false;
            // if(placaIngresada == listPlacas[i] && listDisp[i] == 'SI'){
                if(placaIngresada == listPlacas[i]){
                aComparar = true;
            }     
            // if(placaIngresada != listPlacas[i] || listDisp[i] == 'NO'){
                if(placaIngresada != listPlacas[i]){
                aComparar = false;                
            }
            if(!aComparar){
                isDisp = false;
                msgErrorComp = 1;
                
                //return;
            }
            console.log('DISPONIBLE ' +isDisp);
            if(aComparar){
                msgErrorComp = 0;
                calculoDias();
               if(values.valor > '7')
                 {
                     //console.log("EL VALOR ES MAYOR A 7");
                     //console.log("EL VALOR NORMAL ES "+values.valor*listCosto[i]);
                     let vl = (values.valor*listCosto[i]) + ((values.valor*listCosto[i])*10)/100;
                     //console.log("EL VALOR CON 10% ES "+ (vl));
                     values.valor = (values.valor*listCosto[i]) - ((values.valor*listCosto[i])*10)/100;
                     msgDescuento = 1;
                     
                 }
                 if(values.valor <= '7')
                 {
                     msgDescuento = 0;
                     //console.log("EL VALOR ES MENOR A 7")
                      values.valor = values.valor*listCosto[i];                
                } 
                isDisp = true;

                // if(listDisp[i] == 'SI'){
                //  isDisp = true;  
                // }
                // if(listDisp[i] == 'NO'){
                //     isDisp = false;
                // }

                console.log(isDisp);
                break;
            }
            
            
        }
        


    });
    
    }


    function calculoDias() {
        var fecha = document.getElementById("fechaPrestamo");
        var fechaPrest = new Date(fecha.value);
        var fecha2 = document.getElementById("fechaEntrega");
        var fechaEntr = new Date(fecha2.value);
        if (fechaPrest.getMonth()<= fechaEntr.getMonth()){

            var val = 0;
            while(fechaPrest.getMonth()<= fechaEntr.getMonth())
            {
                val = val + 1;
                fechaPrest.setDate(fechaPrest.getDate()+1);
                console.log("la fecha de entrega es: "+fechaPrest);               
                if(fechaPrest.getDate() === fechaEntr.getDate()&&fechaPrest.getMonth()===fechaEntr.getMonth()){
                break;
                }    
            }             
            
        }
        

        values.valor=val;
        console.log(values.valor);
    }

    return (
        <form align = "center" className="loginContainer" onSubmit={handleSubmit}>

        <div>             
                    </div>
                        <div align = "center">   
                            <label htmlFor="Tittle" style={{fontSize: "50px", color:"#ff0000"}}>REGISTRA UNA NUEVA RESERVA</label>
                        </div>
                        <br/>
                        <br/>
                        
                        <div align = "center">
                        <label htmlFor="placaReserva">PLACA------------------------------------------------------------{'>'}</label>
                        <input  type = "text" id = "placaReserva" value={values.placaReserva} required onChange= {handleInputChange}/> 
                        <br/>
                        <br/>
                        <label htmlFor="email">EMAIL-----------------------------------------------------------{'>'}</label>
                        <input  type = "email" placeholder = "user@provider.com"  id = "email" value={values.email} required onChange= {handleInputChange}/>
                        <br/>
                        <br/>
                        <label htmlFor="celular">CELULAR-------------------------------------------------------{'>'}</label>
                        <input  type = "number" id = "celular" value={values.celular} required onChange= {handleInputChange}/>
                        <br/>
                        <br/>
                        <label htmlFor="fechaPrestamo">FECHA DE PRÉSTAMO--------------------------------------{'>'}</label>
                        <input  type = "date" id = "fechaPrestamo" value={values.fechaPrestamo} required onChange= {handleInputChange}/>
                        <br/>
                        <br/>
                        <label htmlFor="fechaEntrega">FECHA DE ENTREGA----------------------------------------{'>'}</label>
                        <input type = "date" id = "fechaEntrega" value={values.fechaEntrega} required onChange= {handleInputChange}/>
                        <br/>
                        <br/>
                        <br/>
        </div>
        <div align = 'center'>
        <button  style={{width: "800px", borderRadius: "25px"}} className="btn btn-primary btn-block">
            {props.currentId === '' ? 'Reservar':'Actualizar'}
        </button>
        </div>
        </form>

    );

};

export default ReservasForm;