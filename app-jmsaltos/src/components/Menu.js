import React, {useState, useEffect} from 'react';
import Vehiculos from './Vehiculos';
import Reservas from './Reservas';
import BuscarReservas from './BuscarReservas';
import BuscarVehiculo from './BuscarVehiculo';

const Menu = () =>{
    const mostrarVehiculos = () =>{
        document.getElementById('mostrarVehiculos').style.display='block';
        document.getElementById('mostrarReservas').style.display='none';
        document.getElementById('mostrarOpcionesReservas').style.display = 'none';
        document.getElementById('mostrarOpcionesVehiculos').style.display = 'none';
        document.getElementById('buscarReservas').style.display = 'none';
        document.getElementById('buscarVehiculos').style.display = 'none';
    }

    const mostrarReservas = () =>{        
        document.getElementById('mostrarVehiculos').style.display='none';
        document.getElementById('mostrarReservas').style.display='block';
        document.getElementById('mostrarOpcionesReservas').style.display = 'none';
        document.getElementById('mostrarOpcionesVehiculos').style.display = 'none';
        document.getElementById('buscarReservas').style.display = 'none';
        document.getElementById('buscarVehiculos').style.display = 'none';
    }

    const mostrarOpcionesReservas = () =>{
        document.getElementById('mostrarVehiculos').style.display='none';
        document.getElementById('mostrarReservas').style.display='none';
        document.getElementById('mostrarOpcionesReservas').style.display = 'block';
        document.getElementById('mostrarOpcionesVehiculos').style.display = 'none';
        document.getElementById('buscarReservas').style.display = 'none';
        document.getElementById('buscarVehiculos').style.display = 'none';
    }
    
    const mostrarOpcionesVehiculos = () =>{
        document.getElementById('mostrarVehiculos').style.display='none';
        document.getElementById('mostrarReservas').style.display='none';
        document.getElementById('mostrarOpcionesReservas').style.display = 'none';
        document.getElementById('mostrarOpcionesVehiculos').style.display = 'block';
        document.getElementById('buscarReservas').style.display = 'none';
        document.getElementById('buscarVehiculos').style.display = 'none';
    }
    const buscarReservas = () =>{
        document.getElementById('mostrarVehiculos').style.display='none';
        document.getElementById('mostrarReservas').style.display='none';
        document.getElementById('mostrarOpcionesReservas').style.display = 'none';
        document.getElementById('mostrarOpcionesVehiculos').style.display = 'none';
        document.getElementById('buscarReservas').style.display = 'block';
        document.getElementById('buscarVehiculos').style.display = 'none';
    }
    const buscarVehiculos = () =>{
        document.getElementById('mostrarVehiculos').style.display='none';
        document.getElementById('mostrarReservas').style.display='none';
        document.getElementById('mostrarOpcionesReservas').style.display = 'none';
        document.getElementById('mostrarOpcionesVehiculos').style.display = 'none';
        document.getElementById('buscarReservas').style.display = 'none';
        document.getElementById('buscarVehiculos').style.display = 'block';
    }

    return(
    <div>
        <div className = 'menuContainer'>
            <div>
                <button onClick = {mostrarOpcionesVehiculos} style={{width: "250px", borderRadius: "25px", fontSize: "14px"}}>VEHÍCULOS</button>
                <br/>
            </div>
            <div>
                <br/>
                <button onClick = {mostrarOpcionesReservas} style={{width: "250px", borderRadius: "25px", fontSize: "14px"}}>RESERVAS</button>
            </div>
        </div>
        <div id='mostrarOpcionesVehiculos' style = {{display: 'none'}}>
            <br/>
            <div className = 'menuContainer' style = {{ height: '300px'}}>
                <div>
                    <br/>
                    <label htmlFor="email">BUSCAR UN VEHÍCULO POR NÚMERO DE PLACA</label>
                    <br/>
                    <button onClick = {buscarVehiculos} style={{width: "250px", borderRadius: "25px", fontSize: '14px'}}>BUSCAR</button>
                    <br/>
                </div>
                <div>
                    <br/>
                    <label htmlFor="email">VER TODOS LOS VEHÍCULOS</label>
                    <br/>
                    <button onClick = {mostrarVehiculos} style={{width: "250px", borderRadius: "25px", fontSize: '14px'}}>VEHÍCULOS</button>
                </div>
            </div>

        </div>
        <div id='mostrarOpcionesReservas' style = {{display: 'none'}}>
            <br/>
            <div className = 'menuContainer' style = {{ height: '300px'}}>
                <div>
                    <br/>
                    <label htmlFor="email">BUSCAR UNA RESERVA POR PLACA O NÚMERO DE RESERVA</label>
                    <br/>
                    <button onClick = {buscarReservas} style={{width: "250px", borderRadius: "25px", fontSize: '14px'}}>BUSCAR</button>
                    <br/>
                </div>
                <div>
                    <br/>
                    <label htmlFor="email">VER TODAS LAS RESERVAS O CREAR UNA RESERVA</label>
                    <br/>
                    <button onClick = {mostrarReservas} style={{width: "250px", borderRadius: "25px", fontSize: '14px'}}>RESERVAS</button>
                </div>
            </div>

        </div>   
        <div id='mostrarVehiculos' style = {{display: 'none'}}>
            <Vehiculos/>
        </div>
        <br/>
        <div id='buscarVehiculos' style = {{display: 'none'}}>
            <BuscarVehiculo/>
        </div> 
        <div id='mostrarReservas' style = {{display: 'none'}}>
            <Reservas/>
        </div> 
        <br/>
        <div id='buscarReservas' style = {{display: 'none'}}>
            <BuscarReservas/>
        </div>            
        <br/>
        
               
        {/* ))} */}
    </div>);
};
export default Menu;