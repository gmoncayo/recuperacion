import React, {useEffect, useState} from 'react';
import VehiculosForm from "./VehiculosForm";
import {db} from '../firebase';

const Vehiculos = ()=> {
    
    const [vehiculos, setVehiculos]=useState([]);
    const [currentId, setCurrentId]=useState('');
    const [placa, setPlacas]=useState('');

    const addOrEdit = async (VehiculoObject) =>{
        
              
        if(currentId === ''){
            db.collection('Vehiculos').doc().set(VehiculoObject);
        }else{
            await db.collection('Vehiculos').doc(currentId).update(VehiculoObject);
            setCurrentId('');
        }
           
    }

    var auxDisp = 1;

    const onDeleteContacto= async id=>{

        var verDelete = false;
        const docs = [];
        const docsId = [];
        var numDelet = 4;
        let reservRef = db.collection('Reservas');
        const listPlacReservas = [];        
        var tamList = 0;
        reservRef.onSnapshot((querySnapshot) => {            
            querySnapshot.forEach((doc) => {
                listPlacReservas.push(doc.data().placaReserva);
                tamList = tamList+1;
        });
    });
        let placaRef = db.collection('Vehiculos');
        
        var tam = 0;
        placaRef.onSnapshot((querySnapshot) => {
            
            querySnapshot.forEach((doc) => {
                docs.push(doc.data().placa);
                docsId.push(doc.id);
                tam = tam+1;            
        });
                let k = 0;
                let numAux = 0;
                for(k = 0; k < tamList; k++){
                    for(let j = 0; j<tam; j++){
                        let aComparar = false;
                        if(docs[j] == listPlacReservas[k]){
                            aComparar = false;
                            verDelete = false;
                        }if(docs[j] != listPlacReservas[k]){
                            aComparar = true;
                            verDelete = '';
                        }                        


                        if(docsId[j] == id){
                        if(aComparar || verDelete !=false){
                            verDelete = true; 
                            numDelet = 1;                      
                        }if(!aComparar && verDelete == false){
                            verDelete = false;
                            numDelet = 0;
                            alert('NO SE PUEDE ELIMINAR UN VEHÍCULO CON RESERVA');
                            return;
                        }
                    }
                    }                
                    
                }
                if((verDelete) && window.confirm('DESEAS ELIMINAR EL VEHÍCULO CON PLACA '+ vehiculos.placa)){
                db.collection('Vehiculos').doc(id).delete();                
                console.log('se ha eliminado');
                return;
                }              
            });             
        //verDelete = false;
    //db.collection('Vehiculos').doc(id).delete();
    }


    const setDispon = () =>
    {
        const docs = [];
        const docsId = [];

        let reservRef = db.collection('Reservas');
        const listPlacReservas = [];        
        var tamList = 0;
        reservRef.onSnapshot((querySnapshot) => {            
            querySnapshot.forEach((doc) => {
                listPlacReservas.push(doc.data().placaReserva);
                tamList = tamList+1;
        });
    });
        let placaRef = db.collection('Vehiculos');
        
        var tam = 0;
        placaRef.onSnapshot((querySnapshot) => {
            
            querySnapshot.forEach((doc) => {
                docs.push(doc.data().placa);
                docsId.push(doc.id);
                tam = tam+1;            
        });
                let k = 0;
                let numAux = 0;
                for(k = 0; k < tamList; k++){
                    for(let j = 0; j<tam; j++){
                        let aComparar = false;
                        if(docs[j] == listPlacReservas[k]){
                            aComparar = false;
                            auxDisp = 0;
                        }if(docs[j] != listPlacReservas[k]){
                            aComparar = true;
                            auxDisp = auxDisp + 1;
                        }  

                        if(docsId[j] == currentId){
                        if(aComparar){
                            numAux = numAux + 1;                          
                        }if(!aComparar){
                            numAux = 0;
                            return;
                        }
                    }
                    }                 
                    
                    if(auxDisp == 0){
                            //values.disponible = 'NO' ;
                            //console.log('VALOR NEGATIVO  ' + auxDisp);
                            //values.disponible = document.getElementById('disp').value;
                            // document.getElementById('disp').html = 'NO';
                            // document.getElementById('disp').innerText = 'NO';
                            // document.getElementById('disp').text = 'NO';
                            // document.getElementById('dispSi').style.display = 'none';
                            // document.getElementById('dispNo').style.display = 'block';
                            return;
                        }
                        if(auxDisp > 0){
                            //values.disponible = 'SI' ;
                            //console.log('VALOR POSITIVO  ' + auxDisp)
                            //values.disponible = document.getElementById('disp').value;
                            // document.getElementById('disp').html = 'SI';
                            // document.getElementById('disp').innerText = 'SI';
                            // document.getElementById('disp').text = 'SI';
                            // document.getElementById('dispSi').style.display = 'block';
                            // document.getElementById('dispNo').style.display = 'none';
                        }
                }
            }); 
    }




    const getPlacasVehiculos = async () => {
        const docs = [];
        db.collection("Vehiculos").onSnapshot((querySnapshot) => {
            
            querySnapshot.forEach((doc) => {
                //docs.push(doc.data().placa);
                docs.push(doc.data().placa);
                console.log(doc.data().placa);
                //console.log("la lista es: " + docs);  
            });
            setPlacas(docs);                     
        });      
    }
    const getVehiculos = async () => {
        db.collection("Vehiculos").orderBy('fechaFabricacion', 'desc').onSnapshot((querySnapshot) => {
            const docs = [];
            querySnapshot.forEach((doc) => {
                docs.push({...doc.data(), id: doc.id});
            });
            setVehiculos(docs);                   
        });      
    }

    // useEffect(() => {
    //      getVehiculos();
    //      getPlacasVehiculos();
    //  }, []);
      useEffect(()=>{
        getVehiculos();
        if(placa === ''){
            setPlacas({placa});            
        }else{
            getPlacasVehiculos();
        }
      },[]);
    return (
    <div align = 'center' >
    <div>
        <br/>
        <VehiculosForm {...{addOrEdit, currentId, vehiculos}}/>
        <br/>
    </div>        
        <div className="col-lg-0" >
            {vehiculos.map(vehiculo => (
                <div className="card mb-0" key={vehiculo.id} style={{borderRadius: "25px"}}>
                    <div className="card-body">
                    <div >
                        <label style = {{color : '#000'}}>PLACA: </label>
                        <h4>{vehiculo.placa}</h4>
                        <label style = {{color : '#000'}}>MARCA: </label>
                        <h4>{vehiculo.marca}</h4>
                        <label style = {{color : '#000'}}>FECHA DE FABRICACIÓN: </label>
                        <h4>{vehiculo.fechaFabricacion}</h4>
                        <label style = {{color : '#000'}}>TIPO DE VEHÍCULO: </label>
                        <h4>{vehiculo.tipo}</h4>
                        {/* <label style = {{color : '#000'}}>DISPONIBLE: </label>
                        <label style = {{color : '#000'}} onChange = {setDispon} id = "disp"></label> */}
                        <div onChange = {setDispon}>
                        {/* <h4 id = "disp" onChange = {setDispon}></h4> */}
                        <h4 id = "dispSi" style={{display:'none'}} onChange = {setDispon}>SI</h4>
                        <h4 id = "dispNo" style={{display:'none'}} onChange = {setDispon}>NO</h4>
                        </div>
                        <label style = {{color : '#000'}}>COSTO POR DÍA: </label>
                        <h4>{vehiculo.costo}</h4>
                       <div>
                       <button className="material-icons text-danger" style={{width: "80px", borderRadius: "25px", fontSize: '15px'}}
                        onClick={()=>onDeleteContacto(vehiculo.id)}>Eliminar </button>
                        <button className='material-icons' style={{width: "80px", borderRadius: "25px", fontSize: '15px'}}  onClick={() => setCurrentId(vehiculo.id)} onChange = {setDispon}>
                            Editar
                        </button>
                       </div>
                    </div>                        
                    </div>
                </div>
            ))}
        </div>

    </div>
    );
};

export default Vehiculos;