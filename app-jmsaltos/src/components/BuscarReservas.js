import React, {useEffect, useState} from 'react';
import {db} from '../firebase';

const BuscarReservas = (props)=> {
    
    const initialStateValues={
        inputUser: '',
        placaReserva: '',
        NumeroReserva: '',
        email:'',
        celular:'',
        fechaPrestamo:'',
        fechaEntrega: '',
        valor: ''
    };
    const [values, setValues] = useState(initialStateValues);


    const [auxIdInput, setAuxIdInput] = useState('');
    const handleInputChange = e => {
        const {id, value}  =e.target;
        setValues({...values, [id]: value});        
    };

    function obtenerNumPlacaReserv(){
        let reservRef = db.collection('Reservas');
        const listPlacas = [];
        const listReservas = [];
        const listId = [];
        var tamList = 0; 
        reservRef.onSnapshot((querySnapshot) => {            
            querySnapshot.forEach((doc) => {
                listPlacas.push(doc.data().placaReserva);
                listReservas.push(doc.data().NumeroReserva);
                listId.push(doc.id)
                tamList = tamList+1;
        });
        let valInpUser = values.inputUser;
        if(values.inputUser.length == 3){
            values.valInpUser = values.NumeroReserva;
        }
        else if(values.inputUser.length >= 7){
            values.valInpUser = values.placaReserva;
        }
        else{
            alert('HA INGRESADO UN VALOR INCORRECTO');
        }
        
        console.log('placa ingresada: ' + valInpUser);
        let i = 0;
        for(i = 0; i<tamList; i++){
            if(valInpUser == listPlacas[i] || valInpUser == listReservas[i])
            {
                console.log('EL VALOR INGRESADO ES' +valInpUser);
                console.log('EL VALOR EN RELACIÓN ES' +listPlacas[i]);
                let numId = listId[i];
                setAuxIdInput(numId);
            }
            if(valInpUser != listPlacas[i] || valInpUser != listReservas[i])
            {
                setAuxIdInput('');
                setValues({...initialStateValues});
            }
        }
        
    });
    }

    const getReservaById = async (id) =>{
        const doc = await db.collection('Reservas').doc(id).get();
        setValues({...doc.data()})
    }

     useEffect(()=>{
         if(auxIdInput === ''){
             setValues({...initialStateValues});
         }else{
             getReservaById(auxIdInput);
         }
     },[auxIdInput])

    return (
    <div align = 'center' >
    <div align = 'center'>
    </div>
        <div className="col-lg-0">
                <div className="card mb-0" key={values.NumeroReserva} style={{borderRadius: "25px"}}>
                    <div className="card-body">
                    <div >
                        <label htmlFor="email">INGRESAR PLACA O NÚMERO DE RESERVA</label>
                        <br/>
                        <input  type = "text" id = "inputUser" value={values.inputUser} required onChange= {handleInputChange}/>
                        <button onClick = {obtenerNumPlacaReserv} style={{width: "100px", borderRadius: "25px", fontSize: '15px'}}>BUSCAR</button>
                        <br/>
                        <br/>
                        <label style = {{color : '#000'}}>NÚMERO DE PLACA: </label>
                        <h4>{values.placaReserva}</h4>
                        <label style = {{color : '#000'}}>NÚMERO DE RESERVA: </label>
                        <h4>{values.NumeroReserva}</h4>
                        <label style = {{color : '#000'}}>CORREO ELECTRÓNICO: </label>
                        <h4>{values.email}</h4>
                        <label style = {{color : '#000'}}>NÚMERO DE CONTACTO: </label>
                        <h4>{values.celular}</h4>
                        <label style = {{color : '#000'}}>FECHA DE PRÉSTAMO: </label>
                        <h4>{values.fechaPrestamo}</h4>
                        <label style = {{color : '#000'}}>FECHA DE ENTREGA: </label>
                        <h4>{values.fechaEntrega}</h4>
                        <label style = {{color : '#000'}}>VALOR A PAGAR: </label>
                        <h4>{values.valor}</h4>
                        </div>
                    </div>
                </div>
        </div>

    </div>
    );
};

export default BuscarReservas;