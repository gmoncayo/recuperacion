import React, {useState, useEffect} from 'react';
import { db } from '../firebase';

const VehiculosForm = (props)=> {
    
    const initialStateValues={
        placa:'',
        marca:'',
        costo:'',
        fechaFabricacion:'',
        tipo: ''
    };
    const [values, setValues] = useState(initialStateValues);

    var auxDisp = 1;
    var valorDisp = '';
    const handleInputChange = e => {
        const {id, value}  =e.target;
        setValues({...values, [id]: value});
    };    

    const handleSubmit = e =>{
        e.preventDefault();
        TipoAuto();
        const docs = [];

        let isVer = false;
        let placaRef = db.collection('Vehiculos');
        
        var tam = 0;
        placaRef.onSnapshot((querySnapshot) => {
            
            querySnapshot.forEach((doc) => {
                docs.push(doc.data().placa);
                tam = tam+1;            
        });
            let inpPlaca = values.placa;
            const auxPlac = values.placa;
            let i = 0;
                for(i = 0; i < tam; i++){
                    let act = docs[i];
                    if(inpPlaca == act){
                        inpPlaca = '';
                        console.log("Auto existente, no se puede repetir");
                        return;            
                    }if(inpPlaca != act){
                        isVer = true;
                        //console.log("la lista es: " + [docs]);         
                    }
                }


               if(isVer || tam == 0)
                    {
                    values.placa = auxPlac;
                    Disponibilidad();
                    props.addOrEdit(values);                    
                    setValues({...initialStateValues})
                    isVer = false;
                    i = tam+1;
                    return;
                    }    
            }); 

            if(props.currentId != ''){
                Disponibilidad();
                props.addOrEdit(values);                
                setValues({...initialStateValues});
            } 
            console.log("essss"+ [docs]); 
    }

    const Disponibilidad = async () =>{
        var verDelete = false;
        const docsVehic = [];
        const docsId = [];
        const docsDisp = [];
        let reservRef = db.collection('Reservas');
        const listPlacReservas = [];        
        var tamList = 0;
        reservRef.onSnapshot((querySnapshot) => {            
            querySnapshot.forEach((doc) => {
                listPlacReservas.push(doc.data().placaReserva);
                tamList = tamList+1;
        });
    });
        let placaRef = db.collection('Vehiculos');
        
        var tam1 = 0;
        placaRef.onSnapshot((querySnapshot) => {
            
            querySnapshot.forEach((doc) => {
                docsVehic.push(doc.data().placa);
                docsId.push(doc.id);
                docsDisp.push(doc.data().disponible);

                tam1 = tam1+1;            
        });
            
            for(let k = 0; k < tamList; k++){
                for(let j = 0; j<tam1; j++){
                    
                    if(docsId[j] == props.currentId){
                        console.log("aqui el id a comparar "+docsId[j] + " aqui el id ingresado " +props.currentId);
                        if(docsVehic[j] != listPlacReservas[k]){
                            auxDisp = auxDisp + 1;
                            console.log('LA PLACA '+ docsVehic[j]+' NO SE PARECE A ' +listPlacReservas[k]);
                        }  
                         if(docsVehic[j] == listPlacReservas[k]){
                            console.log('LA PLACA '+ docsVehic[j]+' SE PARECE A ' +listPlacReservas[k]);
                            auxDisp = 0;
                        }  
                    }        
                }

            console.log(auxDisp);

            if(auxDisp == 0){
                //values.disponible = 'NO' ;
                // document.getElementById('disp').html = 'NO';
                // values.valorDisp = 
                // console.log('VALOR NEGATIVO  ' + document.getElementById('disp').html);
                //values.disponible = document.getElementById('disp').value;
                // values.valorDisp = document.getElementById('disp').html;
                // console.log('ESTA ES LA DISP '+values.valorDisp);
                return;
            }
            if(auxDisp > 0){
                //values.disponible = 'SI' ;
                // document.getElementById('disp').html = 'SI';
                // console.log('VALOR POSITIVO  ' + document.getElementById('disp').html)
                //values.disponible = document.getElementById('disp').value;
                // values.valorDisp = document.getElementById('disp').html;
                // console.log('ESTA ES LA DISP '+values.valorDisp);
            }

            }            
            
        }); 
    }
    
    const TipoAuto = () =>{
        if(document.getElementById('optAut').selected){
            values.tipo = 'Automóvil';
            values.costo = 55;
        }
        if(document.getElementById('optCam').selected){
            values.tipo = 'Camioneta';
            values.costo = 75;
        }
        if(document.getElementById('optFurg').selected){
            values.tipo = 'Furgoneta';
            values.costo = 130;
        }
    }

    const getContactobyId = async (id) =>{
        const doc = await db.collection('Vehiculos').doc(id).get();
        setValues({...doc.data()});
    }

    useEffect(()=>{
        if(props.currentId === ''){
            setValues({...initialStateValues});
        }else{
            getContactobyId(props.currentId);
        }
    },[props.currentId])

    //----------------------------------------------

    return (
        <form align = "center" className="loginContainer" onSubmit={handleSubmit}>

        <div>             
                    </div>
                    {/* <div align = "right" className="loginContainer"> */}
                        <div align = "center">   
                            <label htmlFor="Tittle" style={{fontSize: "50px", color:"#ff0000"}}>REGISTRA UN NUEVO VEHÍCULO</label>
                        </div>
                        <br/>
                        <br/>
                        <br/> 
                        <div align = "center">                        
                         <label htmlFor="placa">PLACA-----------------------------------------------{'>'}</label>
                        <input  type = "text" id = "placa" value={values.placa} required onChange= {handleInputChange}/>
                        <br/>
                        <br/>
                        <label htmlFor="marca">MARCA----------------------------------------------{'>'}</label>
                        <input  type = "text" id = "marca" value={values.marca} required onChange= {handleInputChange}/>
                        <br/>
                        <br/>
                        <label htmlFor="fechaFabricacion">FECHA DE FABRICACIÓN----------------------{'>'}</label>
                        <input  type = "date" id = "fechaFabricacion" value={values.fechaFabricacion} required onChange= {handleInputChange}/>
                        <br/>
                        <br/>
                        <label htmlFor="costo">COSTO----------------------------------------------{'>'}</label>
                        <input  type = "number" id = "costo" value={values.costo} disabled = {true} onChange= {handleInputChange}/>
                        <br/>
                        <br/>
                        <label htmlFor="tipo">TIPO------------------------------------------------{'>'}</label>
                        <input type = "text" id = "tip" placeholder = 'ESCOJA EL TIPO DE VEHÍCULO---->' value={values.tipo} disabled = {true} style={{width:"250px", fontSize : '12px'}} onChange= {handleInputChange}/>
                        <select name="tipoTransporte" style={{fontSize: "20px", color:"#000000", width:"250px", borderRadius: "25px"}}>
                        <option id='none' selected>--SELECCIONE--</option>
                        <option id='optAut'>Automóvil</option>
                        <option id='optCam'>Camioneta</option>
                        <option id='optFurg'>Furgoneta</option>
                        </select>
                        <br/>
                        <br/>
                        {/* <label htmlFor="disponible">DISPONIBLE---------------------------------------{'>'}</label>
                        <input type = "text" id = "disp" disabled = {true} value = {valorDisp}  style={{fontSize : '12px'}} onChange = {handleInputChange}/> */}
                        {/* <select name="tipoDisponibilidad" style={{fontSize: "20px", color:"#000000", width:"250px", borderRadius: "25px"}}>
                        <option id='none' selected>--SELECCIONE--</option>
                        <option id='idSi'>SI</option>
                        <option id='idNo'>NO</option>
                        </select> */}
                        <br/>
                        <br/>   
        </div>
        <div align = 'center'>
        <button style={{width: "800px", borderRadius: "25px"}} className="btn btn-primary btn-block">
            {props.currentId === '' ? 'Guardar':'Actualizar'}

        </button>
        </div>
        {/* </div> */}
        </form>

    );

};

export default VehiculosForm;

