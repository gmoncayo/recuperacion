import React, {useEffect, useState} from 'react';
import {db} from '../firebase';

const BuscarVehiculo = (props)=> {
    
    const initialStateValues={
        placa:'',
        marca:'',
        costo:'',
        fechaFabricacion:'',
        tipo: ''
    };
    const [values, setValues] = useState(initialStateValues);


    const [auxIdInput, setAuxIdInput] = useState('');

    const handleInputChange = e => {
        const {id, value}  =e.target;
        setValues({...values, [id]: value});   
    };
    var msgDisp = 0;
    const DeterminarDisponibilidad = () =>{
        
        if(values.placa.length >= 7 && values.placa.length<9){
            var verDelete = false;
            const docs = [];
            const docsId = [];
            var numDelet = 4;
            let reservRef = db.collection('Reservas');
            const listPlacReservas = [];        
            var tamList = 0;
            reservRef.onSnapshot((querySnapshot) => {            
                querySnapshot.forEach((doc) => {
                    listPlacReservas.push(doc.data().placaReserva);
                    tamList = tamList+1;
            });
        });
            let placaRef = db.collection('Vehiculos');
            
            var tam = 0;
            placaRef.onSnapshot((querySnapshot) => {
                
                querySnapshot.forEach((doc) => {
                    docs.push(doc.data().placa);
                    docsId.push(doc.id);
                    tam = tam+1;            
            });
                    let k = 0;
                    let numAux = 0;
                    for(k = 0; k < tamList; k++){
                        for(let j = 0; j<tam; j++){
                            let aComparar = false;
                            if(docs[j] == listPlacReservas[k]){
                                aComparar = false;
                                verDelete = false;
                            }if(docs[j] != listPlacReservas[k]){
                                aComparar = true;
                                verDelete = '';
                            }                    
                            if(aComparar || verDelete !=false){
                                verDelete = true; 
                                numDelet = 1;   
                                msgDisp = 2; 
                                return;                  
                            }
                            if(!aComparar && verDelete == false){
                                verDelete = false;
                                numDelet = 0;
                                msgDisp = 1;
                                return;
                            }
                        }                
                        
                    }
                    if(verDelete){
                        msgDisp = 2;
                    return;
                    }              
                });             
    
            }
    }

    function obtenerNumPlacaReserv(){
        DeterminarDisponibilidad();
        let reservRef = db.collection('Vehiculos');
        const listPlacas = [];
        const listId = [];
        var tamList = 0; 
        reservRef.onSnapshot((querySnapshot) => {            
            querySnapshot.forEach((doc) => {
                listPlacas.push(doc.data().placa);
                listId.push(doc.id)
                tamList = tamList+1;
        });
        let valInpUser = values.inputUser;

        console.log('placa ingresada: ' + valInpUser);
        let i = 0;
        let msgError = 0;
        for(i = 0; i < tamList; i++){
            
            if(valInpUser != listPlacas[i])
            {
                setAuxIdInput('');
                setValues({...initialStateValues});
                msgError = msgError+1;
            }
            if(valInpUser == listPlacas[i])
            {
                console.log('EL VALOR INGRESADO ES' +valInpUser);
                console.log('EL VALOR EN RELACIÓN ES' +listPlacas[i]);
                let numId = listId[i];
                setAuxIdInput(numId);
                msgError = 0;
                if(msgDisp == 1 && msgError == 0){
                alert('EL VEHICULO ESTÁ RESERVADO');
                }
                if(msgDisp == 2 && msgError == 0){
                    alert('EL VEHICULO ESTÁ DISPONIBLE');
                }
                return;
            }
        }

        if(msgError>0){
            setValues({...initialStateValues});
            values.inputUser = '';
            alert('NO EXISTE VEHÍCULO CON LA PLACA INGRESADA');
            values.inputUser = '';   
            return;
        }
        
    });
    
    }

    const getReservaById = async (id) =>{
        const doc = await db.collection('Vehiculos').doc(id).get();
        setValues({...doc.data()})
    }

     useEffect(()=>{
         if(auxIdInput === ''){
             setValues({...initialStateValues});
         }else{
             getReservaById(auxIdInput);
         }
     },[auxIdInput])

    return (
    <div align = 'center' >
    <div align = 'center'>
    </div>
        <div className="col-lg-0">
                <div className="card mb-0" key={values.NumeroReserva} style={{borderRadius: "25px"}}>
                    <div className="card-body">
                    <div >
                        <label htmlFor="email">INGRESAR PLACA</label>
                        <br/>
                        <input  type = "text" id = "inputUser" value={values.inputUser} required onChange= {handleInputChange}/>
                        <button onClick = {obtenerNumPlacaReserv} style={{width: "100px", borderRadius: "25px", fontSize: '15px'}}>BUSCAR</button>
                        <br/>
                        <br/>
                        <label style = {{color : '#000'}}>NÚMERO DE PLACA: </label>
                        <h4>{values.placa}</h4>
                        <label style = {{color : '#000'}}>MARCA: </label>
                        <h4>{values.marca}</h4>
                        <label style = {{color : '#000'}}>COSTO: </label>
                        <h4>{values.costo}</h4>
                        <label style = {{color : '#000'}}>FECHA DE FABRICACIÓN: </label>
                        <h4>{values.fechaFabricacion}</h4>
                        {/* <label style = {{color : '#000'}}>DISPONIBLE: </label>
                        <h4>{values.disponible}</h4> */}
                        <label style = {{color : '#000'}}>TIPO: </label>
                        <h4>{values.tipo}</h4>
                        </div>
                    </div>
                </div>
        </div>

    </div>
    );
};

export default BuscarVehiculo;