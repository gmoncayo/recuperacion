import React, {useState, useEffect} from 'react';
import { db } from '../firebase';
import firebase from 'firebase';

const ContactoForm = (props )=> {

    const initialStateValues={

        nombre:'',
        apellido:'',
        fechaNacimiento:'',
        email:'',
        password:''
    };
    const [values, setValues] = useState(initialStateValues);

    const handleInputChange = e => {
        const {id, value}  =e.target;
        setValues({...values, [id]: value})
    };

    const submit =  async () =>{
        let AuxPass = document.getElementById('RepeatPassword').value;
        let contNumInPass = 0;
        if(values.password == AuxPass)        
        {
             let AuxNumPass = [];
             
             for(let i = 0; i < values.password.length ; i++)
             {
                 let a = values.password[i];
                 if(a == 0 || a == 1 || a == 2 || a == 3 || a == 4 || a == 5 || a == 6 || a == 7 || a == 8 || a == 9)
                 {
                    contNumInPass = contNumInPass + 1;
                 }
                 AuxNumPass.push(values.password[i]);
             }
            
            if(values.password.length >=8  && contNumInPass>=2)
            {
               await firebase.auth().createUserWithEmailAndPassword(values.email, values.password).catch(alert('USUARIO YA EXISTENTE!!'));

            }
            if(contNumInPass < 2)
            {
                alert('LA CONTRASEÑA DEBE TENER AL MENOS DOS NÚMEROS');
            }

            if(values.password.length < 8)
            {
                alert('LA CONTRASEÑA DEBE TENER AL MENOS 8 CARACTETES')
            }
        }
        else
        {
            alert('LAS CONTRASEÑAS NO COINCIDEN');
            
        }

    };

    const handleSubmit = e =>{
        e.preventDefault();
        props.addOrEdit(values);
        setValues({...initialStateValues})
    }

    const getContactobyId = async (id) =>{
        const doc = await db.collection('Usuarios').doc(id).get();
        setValues({...doc.data()})
    }

    useEffect(()=>{
        if(props.currentId === ''){
            setValues({...initialStateValues});
        }else{
            getContactobyId(props.currentId);
        }
    },[props.currentId])

    //----------------------------------------------

    return (
        <form align = "center" className="loginContainer" onSubmit={handleSubmit}>

        <div>             
                    </div>
                    {/* <div align = "right" className="loginContainer"> */}
                        <div align = "center">   
                            <label htmlFor="Tittle" style={{fontSize: "50px", color:"#ff0000"}}>¿NO TIENES CUENTA?</label>
                            <br/>
                            <label htmlFor="Tittle2" style={{fontSize: "50px", color:"#fff5@00"}}>REGISTRATE</label>
                        </div>
                        <br/>
                        <br/>
                        <br/> 
                        <div align = "center">
                        
                         <label htmlFor="name">NOMBRE-----------------------------------------------{'>'}</label>
                        <input  type = "text" id = "nombre" value={values.nombre} required onChange= {handleInputChange}/>
                        <br/>
                        <br/>
                        <label htmlFor="lastname">APELLIDO----------------------------------------------{'>'}</label>
                        <input  type = "text" id = "apellido" value={values.apellido} required onChange= {handleInputChange}/>
                        <br/>
                        <br/>
                        <label htmlFor="fechaNacimiento">FECHA DE NACIMIENTO---------------------------{'>'}</label>
                        <input  type = "date" id = "fechaNacimiento" value={values.fechaNacimiento} required onChange= {handleInputChange}/>
                        <br/>
                        <br/>
                        <label htmlFor="userIn">USUARIO-----------------------------------------------{'>'}</label>
                        <input placeholder = "user@provider.com" type = "email" id = "email" required value={values.email} onChange= {handleInputChange}/>
                        <br/>
                        <br/>
                        <label htmlFor="passwornd">CONTRASEÑA----------------------------------------{'>'}</label>
                        <input type = "password" id = "password" value={values.password} required onChange= {handleInputChange}/>
                        <br/>
                        <br/>
                        <label htmlFor="RepeatPassword">REPETIR CONTRASEÑA-----------------------------{'>'}</label>
                        <input type = "password" id = "RepeatPassword"  onChange= {handleInputChange}/>
                        <br/>
                        <br/>   
        </div>
        <div align = 'center'>
        <button onClick = {submit} style={{width: "800px", borderRadius: "25px"}} className="btn btn-primary btn-block">
            {props.currentId === '' ? 'Crear cuenta':'Crear cuenta'}

        </button>
        </div>
        {/* </div> */}
        </form>

    );

};

export default ContactoForm;

