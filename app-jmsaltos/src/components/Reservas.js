import React, {useEffect, useState} from 'react';
import ReservasForm from "./ReservasForm";
import {db} from '../firebase'
import { auth } from 'firebase';

const Reservas = ()=> {
    
    const [reservas, setReservas]=useState([]);
    const [currentId, setCurrentId]=useState('');
    const [unaReserva, setUnaReserva]=useState('');

    const addOrEdit = async (ReservasObject) =>{
       
        if(currentId === ''){
            db.collection('Reservas').doc().set(ReservasObject);
        }else{
            await db.collection('Reservas').doc(currentId).update(ReservasObject);
            setCurrentId('');
        }
           
    }

    // const onDeleteContacto= async id=>{
    //     if(window.confirm('Estas seguro de eliminar este vehículo?')){
    //         await db.collection('Reservas').doc(id).delete();
    //         console.log('Reserva eliminada');
    //     }
    // }
    const getNumeroReservas = async () => {
        const docs = [];
        db.collection("Reservas").onSnapshot((querySnapshot) => {
            
            querySnapshot.forEach((doc) => {
                //docs.push(doc.data().placa);
                docs.push(doc.data().unaReserva);
                console.log(doc.data().unaReserva);
                console.log("la lista es: " + docs);  
            });
            setUnaReserva(docs);                     
        });      
    }
    const getReservas = async () => {
        db.collection("Reservas").orderBy('fechaEntrega', 'desc').onSnapshot((querySnapshot) => {
            const docs = [];
            querySnapshot.forEach((doc) => {
                docs.push({...doc.data(), id: doc.id});
            });
            setReservas(docs);                   
        });      
    }
    useEffect(()=>{
        getReservas();
        if(unaReserva === ''){
            setUnaReserva({unaReserva});            
        }else{
            getNumeroReservas();
        }
    },[]);

    return (
    <div align = 'center' >
    <div align = 'center'>
        <br/>
        <ReservasForm {...{addOrEdit, currentId, reservas}}/>
        <br/>
    </div>
        <div className="col-lg-0">
            {reservas.map(reserva => (
                <div className="card mb-0" key={reserva.id} style={{borderRadius: "25px"}}>
                    <div className="card-body">
                    <div >
                    <label style = {{color : '#000'}}>NÚMERO DE PLACA: </label>
                        <h4>{reserva.placaReserva}</h4>
                        <label style = {{color : '#000'}}>NÚMERO DE RESERVA: </label>
                        <h4>{reserva.NumeroReserva}</h4>
                        <label style = {{color : '#000'}}>CORREO ELECTRÓNICO: </label>
                        <h4>{reserva.email}</h4>
                        <label style = {{color : '#000'}}>NÚMERO DE CONTACTO: </label>
                        <h4>{reserva.celular}</h4>
                        <label style = {{color : '#000'}}>FECHA DE PRÉSTAMO: </label>
                        <h4>{reserva.fechaPrestamo}</h4>
                        <label style = {{color : '#000'}}>FECHA DE ENTREGA: </label>
                        <h4>{reserva.fechaEntrega}</h4>
                        <label style = {{color : '#000'}}>VALOR A PAGAR: </label>
                        <h4>{reserva.valor}</h4>
                        </div>
                    </div>
                </div>
            ))}
        </div>

    </div>
    );
};

export default Reservas;