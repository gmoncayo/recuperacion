import React, {useEffect, useState} from 'react';
import CreateAccountForm from "./CreateAccountForm";
import {db} from '../firebase'

const Usuarios = ()=> {

    const [contactos, setContactos]=useState([]);
    const [currentId, setCurrentId]=useState('');

    const addOrEdit = async (ContactoObject) =>{
        if(currentId === ''){
            await db.collection('Usuarios').doc().set(ContactoObject)
        }
        
    }

    // const onDeleteContacto= async id=>{
    //     if(window.confirm('Estas seguro de eliminar este contacto?')){
    //         await db.collection('Usuarios').doc(id).delete();
    //         console.log('Contacto eliminado');
    //     }
    // }

    const getContactos = async () => {
        db.collection("Usuarios").onSnapshot((querySnapshot) => {
            const docs = [];
            querySnapshot.forEach((doc) => {
                docs.push({...doc.data(), id: doc.id});
            });
            setContactos(docs);         
        });
      
    }

    useEffect(() => {
        getContactos();
                
    }, []);

    return (
    <div>
    <div className="col-md-4">
        <CreateAccountForm {...{addOrEdit, currentId, contactos}}/>

    </div>        
    </div>
    );
};

export default Usuarios;