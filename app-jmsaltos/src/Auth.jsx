import React, {useState, useEffect} from 'react';
import 'firebase/auth';
import { useFirebaseApp, useUser } from 'reactfire';
import Usuarios from './components/CreateUsers';
import Menu from './components/Menu';

export default (props) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const firebase = useFirebaseApp();
    const user = useUser();

    const logout = async () =>{
        await firebase.auth().signOut();
        setEmail('');
        setPassword('');
    }
    const login = async () =>{
        await firebase.auth().signInWithEmailAndPassword(email, password);
    };
    return(
        <div align = "center">
            {
                !user &&
                <div>
                <div className = "margenContainer">
                    <label htmlFor="email">Correo electrónico</label>
                    
                    <input type="email" id = "email" style={{width: "270px"}} onChange= { (ev) => setEmail(ev.target.value)}/>
                    
                    <label htmlFor="password">------------------------------- Contraseña</label>
                    
                    <input type = "password" id = "password" style={{width: "270px"}} onChange= { (ev) => setPassword(ev.target.value)}/>
                    
                    <button onClick = {login} style={{width: "120px", borderRadius: "25px", fontSize: '15px'}}className="btn btn-primary btn-block">Iniciar sesión</button>
                                    
                    </div> 

                   <div align = 'left'>
                   <br/><br/><br/><br/><br/>
                    <Usuarios/>   
            </div>                
            </div> 
            
            }
            {
                user && 
                <div align = 'center'>
                    <div  align = 'left' className = "margenContainer" >
                    <label htmlFor="T1">SISTEMA DE CONTROL DE RESERVA DE VEHÍCULOS--------------------------------------------------------------------------------------------------------</label>
                    <div align = 'right'>
                    <button onClick = {logout} style={{width: "150px", borderRadius: "25px", fontSize: "14px"}}>Cerrar Sesión</button>
                    </div>
                    </div >                    
                    <br/>
                    <br/>
                    <br/>
                    <div className="card mb-0" style={{borderRadius: "25px"}}>
                    <label htmlFor="T1">UNIVERSIDAD CENTRAL DEL ECUADOR</label>
                    <br/>
                    <label htmlFor="T2">FACULTAD DE INGENIERÍA Y CIENCAS APLICADAS</label>
                    <br/>
                    <label htmlFor="T3">INGENIERÍA EN COMPUTACIÓN GRÁFICA</label>
                    <br/>
                    <label htmlFor="T4">OPTATIVA 3</label>
                    <br/>                    
                    <label htmlFor="D">JAROL SALTOS </label>
                    </div>
                    <br/><br/>
                    <div align = "center">  
                        <br/><br/>
                        <Menu/>
                    <br/><br/>
                    
                    <br/><br/>
                    </div>
                </div>

            }  
            


        </div>
    )
}