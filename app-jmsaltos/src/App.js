import React from 'react';
import './App.css';
import Vehiculos from './components/Vehiculos';
import Reservas from './components/Reservas';
import Auth from './Auth';
import {useFirebaseApp} from 'reactfire';
import ReservasForm from './components/ReservasForm';

function App() {
  const firebase = useFirebaseApp();
  console.log(firebase);
  return (
    <div className="container p-4">
      <Auth/>
    </div>
  );
}

export default App;
