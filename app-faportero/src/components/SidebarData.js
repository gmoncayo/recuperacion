import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';

export const SidebarData = [



  {
    title: 'Buscar',
    path: '/Buscar',
    icon: <IoIcons.IoIosPaper />,
    cName: 'nav-text'
  },



  {
    title: 'Administración',
    path: '/Links',
    icon: <IoIcons.IoIosPaper />,
    cName: 'nav-text'
  },
  {
    title: 'Reservas',
    path: '/LinksR',
    icon: <FaIcons.FaCartPlus />,
    cName: 'nav-text'
  },
  ];