import React, {useEffect, useState} from 'react';
import LinkForm from './LinkForm';
import {db} from '../config/Fire';
//import {db} from './config/Fire';
//import fire from '../config/Fire';
import {toast} from 'react-toastify'
import firebase from 'firebase'

const Links = () => {

const [links,setLinks] = useState([])    
const [currentId, setCurrentId] = useState("");

const  logout =() => {
    firebase.auth().signOut();
}

const addOrEditLink = async (linkObject) =>{
    try {
        
        if(currentId === ''){
            await db.collection('Autos').doc().set(linkObject);
            toast('Auto Agregado', {
            type:'success',
            autoClose: 2000,
            }); 
        } else{
            await db.collection('Autos').doc(currentId).update(linkObject);
            toast('Auto Actualizado', {
                type:'info',
                autoClose: 2000,
                });
            setCurrentId('');    
        }

            } catch (error) {
                console.error(error);                    
    }
};

const onDeleteLink = async id =>{

/*
  let disp = db.collection('Autos')
  var disponible = false
  disp.onSnapshot((querySnapshot) =>{
      querySnapshot.forEach((doc) => {             
       if(doc.data().disponible ==="disponible") {
         disponible = true;
         }       
      });
  })
*/



const doc = await db.collection('Autos').doc(id).get();
await db.collection('Autos').doc(id).get();
if(doc.disponible === 'no-disponible'){

  if(window.confirm ('Estas seguro de querer eliminar este Auto?')){
       await db.collection('Autos').doc(id).delete();
       toast('Auto Eliminado', {
        type:'error',
        autoClose: 2000,
        })  
    }
  }
};
const getLinks = async () =>{

  //Ordenar por fecha de Fabricacion
 db.collection('Autos').orderBy("fechaFabricacion", "asc").onSnapshot((querySnapshot) =>{
     const docs =[];
    querySnapshot.forEach(doc =>{
      docs.push({...doc.data(), id:doc.id});
        //  console.log(doc.data())
       // console.log(doc.id)
 });
    setLinks(docs);
  })
    
}

    useEffect(() =>{
        getLinks();
    }, []);
    return (
    <div>    
        <div >
        <LinkForm {...{addOrEditLink, currentId, links}}/>
        </div>
        <div >
            {links.map(link => (
              <div className="card mb-1" key={link.id}>
               <div className="card-body ">
                  <div className="form-group input-group ">
                  <div>
                  <i className="material-icons text-danger"
                  onClick={() => onDeleteLink(link.id)}>close</i>

                  <i className="material-icons"
                  onClick={() => setCurrentId(link.id)}>create</i>
                 </div> 
                 
                  <div className="input-group-text gb-dark">              
                    <div><label>No de Placa:</label></div>                   
                  </div>
                  <p className="p-3">{link.placa}</p>
                  </div>
                  
                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Marca:</label></div>                   
                  </div>
                  <p className="p-3">{link.marca}</p>
                  </div>
                  

                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Tipo:</label></div>                   
                  </div>
                  <p className="p-3">{link.tipo}</p>
                  </div>

                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Fecha de Fab:</label></div>                   
                  </div>
                  <p className="p-3">{link.fechaFabricacion}</p>
                  </div>

                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Disponibilidad:</label></div>                   
                  </div>
                  <p className="p-3">{link.disponible}</p>
                  </div>

                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Costo:</label></div>                   
                  </div>                  
                  <p className="p-3">$</p>
                  <p className="p-3">{link.costo}</p>                  
                  </div>

                  </div>
              </div>  
            ))}
        </div>
        <button onClick={logout} className="form-control">Cerrar Sesion</button>
    </div>
    );
};

export default Links;
