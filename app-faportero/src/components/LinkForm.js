import React, {useState, Component, useEffect} from 'react'
import fire, { db } from '../config/Fire';

import firebase from 'firebase'



const LinkForm =(props) =>{

    const initialStateValues ={
        placa: '',
        marca: '',
        fechaFabricacion: '',
        costo: '',
        disponible: 'disponible',
        tipo: 'automovil'
    }

   const [values, setValues] = useState(initialStateValues);
   
   const handleInputChange = (e) => {
      console.log(e.target.value)
    const {name, value} = e.target;
    setValues({...values, [name]: value})

   }


    const  handleSubmit = (e) =>{

        e.preventDefault();
      // console.log(values)
        props.addOrEditLink(values);
        setValues({...initialStateValues})
    }
  
    const getLinkById = async (id) => {
        const doc = await db.collection('Autos').doc(id).get();
        setValues({...doc.data()})
    }

    useEffect(() => {
        if(props.currentId ===""){
            setValues({...initialStateValues});    
        } else {            
            getLinkById(props.currentId);
        }
    }, [props.currentId]);

    return(        
        
        <form className="card card-body" onSubmit={handleSubmit}>
            <div> <h1 className="form-group"> 
            <i className="material-icons">directions_car</i>
            Administración            
            </h1>           
            </div>        
            <div className="form-group input-group">
            <div className="input-group-text bg-dark">
                <i className="material-icons">create</i>
            </div>
               
               <input 
               type="text"
               className="form-control"
               placeholder="Ingrese el No de Placa"
               name="placa" 
               onChange={handleInputChange}
               value={values.placa}              
               />          
                   
            </div>
            <div className="form-group input-group">
                <div className="input-group-text gb-dark">
                <i className="material-icons">create</i>  
                </div>
                
                <input 
               type="text"
               className="form-control"
               placeholder="Ingrese la Marca del Vehiculo"
               name="marca"
               onChange={handleInputChange}
               value={values.marca}                 
               />

            </div>

            <div className="form-group input-group">
                <div className="input-group-text gb-dark">                
                <i className="material-icons">create</i>  
                <div><label> Fecha de Fabricacion:</label></div>
                </div>                
                <input 
               type="Date"
               className="form-control"
               name="fechaFabricacion"   
               onChange={handleInputChange}  
               value={values.fechaFabricacion}            
               />              

            </div>
            <div className="form-group input-group">
                <div className="input-group-text gb-dark">
                <i className="material-icons">create</i>  
                </div>
                
                <input 
               type="number"
               step="0.01"
               min="0"
               className="form-control"
               placeholder="Ingrese el Costo del Vehículo"
               name="costo"   
               onChange={handleInputChange}
               value={values.costo}              
               />

            </div>

            <div className="form-group input-group">
                <div className="input-group-text gb-dark">
                <i className="material-icons">create</i>               
                <label htmlFor="tipo">Elija el Tipo:</label>
                <select id="tipo" name="tipo"  
                onChange={handleInputChange} value={values.tipo} >
                    <option value="automovil">Automóvil</option>
                    <option value="camioneta">Camioneta</option>
                    <option value="furgoneta">Furgoneta</option>                    
                </select> 

          
            </div>

            </div>
            <div  className="form-group">
           
            <button className="btn btn-prymary btn-block gb-light"> 
            {props.currentId === '' ? 'Guardar' : 'Actualizar'}
            </button>
           
            </div>      
               
         
    
    

        </form>
    )
    //<button onClick={this.logout} className="form-control">Cerrar Sesion</button>

}

export default LinkForm;




