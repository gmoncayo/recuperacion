import React, {Component, useState} from 'react';
import fire from '../config/Fire';
import {db} from '../config/Fire';
import { toast } from 'react-toastify'

import firebase from 'firebase'


class LoginRegistro extends Component {
    constructor(props){
        
        super(props);
        this.state ={
            email: '',
            password: '',
            nombre: '',
            apellido: '',
            fechaNacimiento:'',
            fireErrors: '',
            formTitle: 'Login',
            vpassword :'',
            loginBtn: true           

        };
       

    }

 



handleChange = e => {

    // validar si las contrasenas coinciden
    var coincide = false;
    if(document.getElementById("password").value === document.getElementById("vpassword").value){
       // console.log('coincide')
       coincide = true;
       toast('Las Contrasenas  coinciden',{
            type: 'info',
            autoClose: 2000,
        }) 
    }else{
      //  console.log('no coincide')
      coincide = false
      toast('Las Contrasenas no coinciden',{
        type: 'error',
        autoClose: 2000,
    }) 
    }
  

    //validar longitud del password
 var passlen = document.getElementById("password").value
    if (passlen.length <= 8) {
        console.log('sssssssssss')
        toast('La contraseña debe tener almenos 8 caracteres',{
            type: 'error',
            autoClose: 2000,
        })
    }    


    
    this.setState({[e.target.name]: e.target.value});  
     


    }

login = e => {

    e.preventDefault();
   
    firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
    .catch((error) =>{
        this.setState({fireErrors: error.message})
    });
}

register = e => {

    e.preventDefault();
    firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
    .catch((error) =>{
        this.setState({fireErrors: error.message})
    });
}



getAction = action => {   
   
//Mostrar campos adicionales de registro
 //console.log('aaaaaaaaaaaa')
    document.getElementById("nombreR").style.display = "none";  
    if(action === 'reg'){
    
        var x = document.getElementById("nombreR");            
         if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
           
        this.setState({formTitle: 'Registrar Nuevo Usuario', loginBtn: false, fireErrors: '', email: '', password: '', vpassword: ''});                 

    }else{

        this.setState({formTitle: 'Login', loginBtn: true, fireErrors: ''});      

    }


    
}   




  render(){




    let errorNotification = this.state.fireErrors ?
       
    (<div className = "Error">{this.state.fireErrors}</div>) : 
    null;

    let submitBtn = this.state.loginBtn ?
    
        (<input className="LoginBtn" type="submit" onClick={ this.login } value="Enter" />):
        (<input className="LoginBtn" type="submit" onClick={this.register} value="Registrar" />);

    let login_register = this.state.loginBtn ?

  

        (<button className="registerBtn" onClick={() => this.getAction('reg')}>Registrar</button>) :
        (<button className="registerBtn" onClick={() => this.getAction('login')}>Login</button>);

    return(
        
      <div className="form_block">
          <div id="title">{this.state.formTitle}</div>
          <div className="body">
              {errorNotification}
              <form>
                  <input type="text"
                  value={this.state.email}
                  onChange={this.handleChange}
                  name="email"
                  placeholder = "Usuario"/>

                  <input type="password"
                  id = 'password'
                  value={this.state.password}
                  onChange={this.handleChange}
                  name="password"
                  placeholder = "Contraseña"/>

                <div id="nombreR">
                


                <input type="validpassword"
                 id = 'vpassword'
                  value={this.state.vpassword}
                  onChange={this.handleChange}
                  name="vpassword"
                  placeholder = "Escriba de nuevo la Contraseña"/>              


                  <input type="text"
                  value={this.state.nombre}
                  onChange={this.handleChange}
                  name="nombre"
                  placeholder = "Nombre"/>

                  <input type="text"
                  value={this.state.apellido}
                  onChange={this.handleChange}
                  name="apellido"
                  placeholder = "Apellido"/>

             
               <label for="fecha" >Fecha de Nacimiento:</label>

                  <input type="date"
                  value={this.state.fechaNacimiento}
                  onChange={this.handleChange}
                  name="fechaNacimiento"
                  placeholder = "Fecha de Nacimiento"/>    
               
                </div>                             
                  { submitBtn}
              </form>
                  {login_register}
          </div>
      </div>
    );

}
}

export default LoginRegistro;
