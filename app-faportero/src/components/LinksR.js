import React, {useEffect, useState} from 'react';
import LinksFormR from './LinksFormR';
import {db} from '../config/Fire';
//import {db} from './config/Fire';
//import fire from '../config/Fire';
import {toast} from 'react-toastify'
import firebase from 'firebase'

const LinksR = () => {

const [linksR,setLinks] = useState([])    
const [currentId, setCurrentId] = useState("");


  const  logout =() => {
        firebase.auth().signOut();
    }



const addOrEditLink = async (linkObject) =>{
    try {
        
        if(currentId === ''){
            await db.collection('Reservas').doc().set(linkObject);
            toast('Reserva Agregada', {
            type:'success',
            autoClose: 2000,
            }); 
        } else{
            await db.collection('Reservas').doc(currentId).update(linkObject);
            toast('Reserva Actualizada', {
                type:'info',
                autoClose: 2000,
                });
            setCurrentId('');    
        }

            } catch (error) {
                console.error(error);                    
    }
};

const onDeleteLink = async id =>{

  

    if(window.confirm ('Estas seguro de querer eliminar esta Reserva?')){
       await db.collection('Reservas').doc(id).delete();
       toast('Reserva Eliminada', {
        type:'error',
        autoClose: 2000,
        })  
    }
};


const getLinks = async () =>{
 // const querySnapshot = await db.collection('Reservas').get();
 db.collection('Reservas').onSnapshot((querySnapshot)=>{
     const docs =[];
    querySnapshot.forEach(doc =>{
      docs.push({...doc.data(), id:doc.id});
        //  console.log(doc.data())
       // console.log(doc.id)
 });
    setLinks(docs);
  })    
}

    useEffect(() =>{
        getLinks();
    }, []);
    return (
    <div>    
        <div >
        <LinksFormR {...{addOrEditLink, currentId, linksR}}/>
        </div>
        <div >
            {linksR.map(link => (
              <div className="card mb-1" key={link.id}>
               <div className="card-body">
                  <div className="form-group input-group ">
                  <div>
                  <i className="material-icons text-danger"
                  onClick={() => onDeleteLink(link.id)}>close</i>

                  <i className="material-icons"
                  onClick={() => setCurrentId(link.id)}>create</i>
                 </div> 
                 
                 <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>No de Reserva:</label></div>                   
                  </div>                  
                  <p className="p-3"></p>
                  <p className="p-3">{link.nReserva}</p>                  
                  </div>

                  </div>
                  
                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Mail:</label></div>                   
                  </div>
                  <p className="p-3">{link.mail}</p>
                  </div>
                  

                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Celular:</label></div>                   
                  </div>
                  <p className="p-3">{link.celular}</p>
                  </div>

                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Fecha de Entrega:</label></div>                   
                  </div>
                  <p className="p-3">{link.fechaEntrega}</p>
                  </div>

                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Fecha de Préstamo:</label></div>                   
                  </div>
                  <p className="p-3">{link.fechaPrestamo}</p>
                  </div>

                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Valor de Reserva:</label></div>                   
                  </div>                  
                  <p className="p-3">$</p>
                  <p className="p-3">{link.valorReserva}</p>                  
                  </div>

                  </div>
              </div>  
            ))}
        </div>
        <button onClick={logout} className="form-control">Cerrar Sesion</button>
    </div>
    );
};

export default LinksR;
