import React from 'react';
import Navbar from './Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
//import Home from './Home';
import Links from './Links';
import LinksR from './LinksR';
import Buscar from './Buscar';

function Menu() {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
        <Route path='/Buscar' component={Buscar} />
          <Route path='/Links' component={Links} />
          <Route path='/LinksR' component={LinksR} />
        </Switch>
      </Router>
    </>
  );
}

export default Menu;