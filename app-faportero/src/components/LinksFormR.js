import React, { useState, Component, useEffect } from 'react'
import fire, { db } from '../config/Fire';
import { toast } from 'react-toastify'
import firebase from 'firebase'



const LinkFormR = (props) => {

    const initialStateValues = {
        placa: '',
        nReserva: '',
        mail: '',
        celular: '',
        fechaEntrega: '',
        fechaPrestamo: '',
        valorReserva: ''
    }

    const [values, setValues] = useState(initialStateValues);

    /*
       function calcularValor(contdias){
        var fechaini = new Date(document.getElementById('dateini').value);
        var fechafin = new Date(document.getElementById('datefin').value);
        var diasdif= fechafin.getTime()-fechaini.getTime();
        var contdias = Math.round(diasdif/(1000*60*60*24));
        //alert(contdias);
    }
    */

    const handleDateChange = (e) => {
        // console.log(e.target.value)
        const { name, value } = e.target;
      //  setValues({ ...values, [name.fechaPrestamo]: value.valorReserva })
    }


    const handleInputChange = (e) => {
        // console.log(e.target.value)
        const { name, value } = e.target;
        setValues({ ...values, [name]: value })
    }

    const validateEmail = (str) => {
        return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
            .test(str);

    }
    
       //Validar si existe la placa  
       let placa = db.collection('Autos')
       var existePlaca = false
       placa.onSnapshot((querySnapshot) =>{
           querySnapshot.forEach((doc) => {                
              // console.log(doc.data().placa)
               //console.log(document.getElementById("placaId").value)
              if(doc.data().placa === document.getElementById("placaId").value)  {
                 // console.log('EXISTE')
                  existePlaca = true;
              }             
           });
       })

       //validar disponibilidad       
       let disp = db.collection('Autos')
       var estaDisponible = false
       disp.onSnapshot((querySnapshot) =>{
           querySnapshot.forEach((doc) => {                
              // console.log(doc.data().placa)
              // console.log(document.getElementById("placaId").value)
              if(doc.data().disponible ==="disponible") {
                // console.log('esta disponible')
                  estaDisponible = true;
              }       
           });
       })


    const handleSubmit = (e) => {


        e.preventDefault();
   
        //validar placa
        if(!existePlaca){
            return toast('La Placa ingresada no existe',{
                type: 'error',
                autoClose: 2000,
            }) 
        }

        //validar si esta disponible
        if(!estaDisponible){
            return toast('El Auto no esta disponible',{
                type: 'error',
                autoClose: 2000,
            }) 
        }

        //validar mail
        if (!validateEmail(values.mail)) {
            return toast('Mail no Válido',{
                type: 'error',
                autoClose: 1000,
            })
        }       
        
        // validar Telefono
        if (values.celular.length < 10 || values.celular.length > 10 ) {
            return toast('Número no válido, debe contener 10 números',{
                type: 'error',
                autoClose: 3000,
            })
        }    
        
    // console.log(values)
        props.addOrEditLink(values);
        props.addOrEditLink(values.disponible = 'no-disponible');
        setValues({ ...initialStateValues })
      
    // Actualiza la disponibilidad del Auto a "No-disponible"
 
        const getAutoById = async (id) => {
        const doc = await db.collection('Autos').doc(id).get();
        setValues({ ...doc.data(props.value.disponible ="no-disponible")})
    }


    }

    const getLinkById = async (id) => {
        const doc = await db.collection('Autos').doc(id).get();
        setValues({ ...doc.data() })
    }

    useEffect(() => {
        if (props.currentId === "") {
            //console.log(props.currentId)
            setValues({ ...initialStateValues });
        } else {
            // console.log(props.currentId)
            getLinkById(props.currentId);
        }
    }, [props.currentId]);

    const getDate = async (fechaPrestamo) => {
        const doc = await db.collection('Autos').doc(fechaPrestamo).get();
    }

    function nRandomReserva(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;

      }


    return (

        <form className="card card-body" onSubmit={handleSubmit}>
            <div> <h1 className="form-group">
                <i className="material-icons">directions_car</i>
            Reservas
            </h1>
            </div>
            <div className="form-group input-group">
                <div className="input-group-text bg-dark">
                    <i className="material-icons">create</i>
                </div>

                <input
                    id = "placaId"
                    type="text"
                    className="form-control"
                    placeholder="Ingrese el No de Placa"
                    name="placa"
                    onChange={handleInputChange}
                    value={values.placa}
                />

            </div>

            <div className="form-group input-group" >
                <div className="input-group-text bg-dark">
                    <div><label> No de Reserva:</label></div>
                </div>
                <div className="p-2">
                    <input
                        type="number"
                        name="nReserva"
                        onChange={handleInputChange}
                        value={values.nReserva=nRandomReserva(1000,100)}
                        disabled
                    />
                   
                </div>
            </div>

            <div className="form-group input-group">
                <div className="input-group-text bg-dark">
                    <i className="material-icons">create</i>
                </div>

                <input
                    type="text"
                    className="form-control"
                    placeholder="Ingrese el mail"
                    name="mail"
                    onChange={handleInputChange}
                    value={values.mail}
                />

            </div>

            <div className="form-group input-group">
                <div className="input-group-text gb-dark">
                    <i className="material-icons">create</i>
                </div>

                <input
                    type="number"
                    className="form-control"
                    placeholder="Ingrese el No de Celular"
                    name="celular"
                    onChange={handleInputChange}
                    value={values.celular}
                />

            </div>

            <div className="form-group input-group">
                <div className="input-group-text gb-dark">
                    <i className="material-icons">create</i>
                    <div><label> Marque la Fecha del Préstamo:</label></div>
                </div>
                <input
                    id="dateini"
                    type="Date"
                    className="form-control p-4"
                    name="fechaPrestamo"
                    onChange={handleInputChange}
                    value={values.fechaPrestamo}
                />
            </div>

            <div className="form-group input-group">
                <div className="input-group-text gb-dark">
                    <i className="material-icons">create</i>
                    <div><label> Marque la Fecha de Entrega:</label></div>
                </div>
                <input
                    id="datefin"
                    type="Date"
                    className="form-control p-4"
                    name="fechaEntrega"
                    onChange={handleInputChange}
                    value={values.fechaEntrega}
                />
            </div>

            <div className="form-group input-group" >
                <div className="input-group-text bg-dark">
                    <div><label> Valor de Reserva:</label></div>
                </div>
                <div className="p-2">
                    <input
                        type="number"
                        name="nReserva"
                        onChange={handleInputChange}
                        value={values.valorReserva}
                        disabled
                    />
                   
                </div>
            </div>

            <div className="form-group">

                <button className="btn btn-prymary btn-block gb-light">
                    {props.currentId === '' ? 'Reservar' : 'Actualizar'}
                </button>

            </div>

        </form>
    )

}

export default LinkFormR;




