import React, {useEffect, useState} from 'react';
import LinkForm from './LinkForm';
import {db} from '../config/Fire';
//import {db} from './config/Fire';
//import fire from '../config/Fire';
import {toast} from 'react-toastify'
import firebase from 'firebase'

const Links = () => {

const [links,setLinks] = useState([])    
const [currentId, setCurrentId] = useState("");

const  logout =() => {
    firebase.auth().signOut();
}


const getLinks = async () =>{

  //Ordenar por fecha de Fabricacion
 db.collection('Autos').orderBy("fechaFabricacion", "asc").onSnapshot((querySnapshot) =>{
     const docs =[];
    querySnapshot.forEach(doc =>{
      docs.push({...doc.data(), id:doc.id});
        //  console.log(doc.data())
       // console.log(doc.id)
 });
    setLinks(docs);
  })
    
}


    useEffect(() =>{
        getLinks();
}, []);
    
    
       //Validar si existe la placa  
       let placa = db.collection('Autos')
       var existePlaca = false
       placa.onSnapshot((querySnapshot) =>{
           querySnapshot.forEach((doc) => {                
              // console.log(doc.data().placa)
               //console.log(document.getElementById("placaId").value)
              if(doc.data().placa === document.getElementById("nPlaca").value)  {
                 // console.log('EXISTE')
                  existePlaca = true;
              }             
           });
       })

    return (       


    <div >    
 
        <div >
        <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                                  
                  </div>
                  <input id='nPlaca' className="p-3" type='text' placeholder='Ingrese el No de Placa'></input>
                  <div><button className='form-control p-6' >Buscar:</button></div>
                  </div>                             
                                     
        </div>

            <div id='buscar' className='buscar'>
            <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Marca:</label></div>                   
                  </div>
                <p className="p-3">{links.map.marca}</p>
                  </div>

</div>

        <div id='lista'>
            {links.map(link => (
              <div className="card mb-1" key={link.id}>
               <div className="card-body ">
                  <div className="form-group input-group ">
          
                  <div className="input-group-text gb-dark">              
                    <div><label>No de Placa:</label></div>                   
                  </div>
                  <p className="p-3">{link.placa}</p>
                  </div>
                  
                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Marca:</label></div>                   
                  </div>
                  <p className="p-3">{link.marca}</p>
                  </div>
                  

                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Tipo:</label></div>                   
                  </div>
                  <p className="p-3">{link.tipo}</p>
                  </div>

                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Fecha de Fab:</label></div>                   
                  </div>
                  <p className="p-3">{link.fechaFabricacion}</p>
                  </div>

                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Disponibilidad:</label></div>                   
                  </div>
                  <p className="p-3">{link.disponible}</p>
                  </div>

                  <div className="form-group input-group">
                  <div className="input-group-text gb-dark">              
                    <div><label>Costo:</label></div>                   
                  </div>                  
                  <p className="p-3">$</p>
                  <p className="p-3">{link.costo}</p>                  
                  </div>

                  </div>
              </div>  
            ))}
        </div>
    </div>
    );
};

export default Links;
