import React, {Component} from 'react';
import {db} from './config/Fire';
import LoginRegistro from './components/LoginRegistro';
import LinkForm from './components/LinkForm';
import './App.css';
import Links from './components/Links';
import LinksR from './components/LinksR';
import Menu from './components/Menu';
import LinkFormR from './components/LinksR';
import {ToastContainer} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'


import firebase from 'firebase'

class App extends Component {

constructor(){
  super();
  // Este estado indica que el usuario no ha iniciado sesion
  this.state ={
    user: null
  }
}

componentDidMount(){
  this.authListener();
}
//verifica el estado del usuario
  authListener(){
    firebase.auth().onAuthStateChanged((user) => {
      if(user){
        this.setState({user});
      }else{
        this.setState({user:null});
      }
    });
  }

  render(){
    return(
      <div className="container p-5">
        <div className="row">

        {this.state.user ? (<Menu/>) :  (<LoginRegistro />)}
     
        </div>
       <ToastContainer/> 
      </div>
    );
  }
}
export default App;